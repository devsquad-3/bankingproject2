import React from 'react'
import { useHistory } from 'react-router'
import { Avatar, Button, TextField, Link, Grid, Typography, Container,Box} from '@material-ui/core/';
import { makeStyles } from '@material-ui/core/styles';
import data from './data2.json';
import emailjs from 'emailjs-com';



export default function ForgotPassword() {
    var history = useHistory();
    var accountList=data.Accounts
    const db_Email=accountList.map(account=>account.Email)
    var name="";
    var Email="";
    var userid="";
    const checkChange=(event)=>{
        var email1=document.getElementById("email").value;
        var email2=document.getElementById("email2").value;
        console.log(email1+"||"+email2)
        console.log(email1!==email2)
        const emailRegex=/^(([^<>()\]\\.,;:\s@"]+(\.[^<>()\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        
        if(!emailRegex.test(email1)){
            document.getElementById("emailCheck1").innerHTML="Please enter valid email";
        }
        if(!db_Email.includes(email1)){
            document.getElementById("emailCheck1").innerHTML="Sorry, Email does not exist.Please create a new account";
        }
       if(email1!==email2){
           document.getElementById("emailCheck2").innerHTML="Please re-enter email";
       }
       else{
           document.getElementById("emailCheck2").innerHTML="";
           document.getElementById("emailCheck1").innerHTML="";
       }
        
    }
    var handleSubmitInfo = (e) => {
        e.preventDefault(); 
        var email=document.getElementById("email2").value;
        if(!db_Email.includes(email)){
            alert("Invalid address in database")
        }
        else{
        for(var i=0; i<data.Accounts.length;i++){
            
            if(data.Accounts[i].Email===email){
                console.log(data.Accounts[i].Username+" "+data.Accounts[i].Email)
                 userid=data.Accounts[i].id;
                 name=data.Accounts[i].Username;
                 Email=data.Accounts[i].Email;
            }
        }    
            
            const sendEmail={
                name: name,
                message:"http://localhost:3000/ResetPassword"+"&userid="+userid,
                Email: Email
            };
            e.preventDefault();
        emailjs.send('verification_service','template_yyvv3zt', sendEmail,'user_9O9UCsHHzTZrD3bO3mHjj')
        .then((result) => {
                console.log(result.text);
                }, (error) => {
                 console.log(error.text);
                 });
        history.push("/Confirm");
                }

    }

    var handleLoginSwitch = (e) => {

        history.push("/LoginPage");
        e.preventDefault();

    }

    const useStyles = makeStyles((theme) => ({

        root:{
            backgroundColor: 'orange',
            height: '100vh'
         
        },
        paper: {
            marginTop: theme.spacing(8),
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            backgroundColor: 'orange'
        },
        avatar: {
            margin: theme.spacing(1),
            backgroundColor: theme.palette.primary.main,
        },
        form: {
            width: '100%', 
            marginTop: theme.spacing(1),
            
        },
        submit: {
            margin: theme.spacing(3, 0, 2),
            backgroundColor: '#9A9A9A'
        },
        cols:{
            backgroundColor: 'orange'
        },
        error1:{
            color:'red'
        },
    }));

    const classes = useStyles();


    return (
        <Grid container className={classes.root}>
        <Container  component='main' maxWidth="xs">
           

            <div className={classes.paper}>
                <Avatar className={classes.avatar}>
                    Opt
                </Avatar>
                <Typography component="h1" variant="h3">
                    Tell us your email so we can send you a verification!
                </Typography>
                <form className={classes.form}>
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        id="email"
                        label="Email Address"
                        onChange={checkChange}
                    />
                     <Box id="emailCheck1" className={classes.error1} onChange={checkChange}></Box>
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        label="Confirm Email Address"
                        id="email2"
                        onChange={checkChange}

                    />
                     <Box id="emailCheck2" className={classes.error1} onChange={checkChange}></Box>
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                        onClick={handleSubmitInfo}
                    >
                        Send Me a Verification Now!
                    </Button>
                    <Grid container>
                        <Grid item xs>
                        </Grid>
                        <Grid item>
                            <Link href="#" variant="body2" onClick={handleLoginSwitch}>
                                {"Already have a account? Log in here"}
                            </Link>
                        </Grid>
                    </Grid>
                </form>
            </div>

        </Container>
        </Grid>
       
    );
}