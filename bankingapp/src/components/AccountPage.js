import React from 'react';
import { useHistory } from 'react-router';
import { Grid, Link, Typography, AppBar, Toolbar, Container, Paper } from '@material-ui/core/';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import { useState, useEffect } from 'react'


const useStyles = makeStyles((theme) => ({

    appBarSpacer: theme.mixins.toolbar,
    content: {
        flexGrow: 1,
        height: '100vh',
        overflow: 'auto',
        
    },
    container: {
        paddingTop: theme.spacing(4),
        paddingBottom: theme.spacing(1),
    },
    paper: {
        padding: theme.spacing(1),
        display: 'flex',
        overflow: 'auto',
        flexDirection: 'column',
        alignItems: 'center',
        backgroundColor: '#ff8b66',
       

    },
    fixedHeight: {
        height: 400,
    },
    cols:{
        backgroundColor: '#ff6e40'
    }

    



}));








export default function AccountPage() {

    var history = useHistory();
    const classes = useStyles();
    const fixedHeightPaper = clsx(classes.paper, classes.fixedHeight);





    var logOut = (e) => {

        history.push("/LogoutPage");
        e.preventDefault();
    }
    var [date, setDate] = useState(new Date());

    useEffect(() => {
        var timer = setInterval(() => setDate(new Date()), 1000)
        return function cleanup() {
            clearInterval(timer)
        }

    });




    return (
        <div>
            <AppBar position="fixed" className={classes.cols} >
                <Toolbar>
                    <Typography component="h1" variant="h6"  >
                        Dashboard
                    </Typography>
                </Toolbar>
            </AppBar>


            <div className={classes.appBarSpacer} />
            <Container maxWidth="lg" className={classes.container} >
                {/* this is the main grid  */}
                <Grid container spacing={3}>

                    {/* first top left box on the page */}
                    <Grid item xs={12} md={8} lg={9}>
                        <Paper className={fixedHeightPaper}>
                            <Typography>
                                Welcome back to your Dashboard!
                            </Typography>
                        </Paper>
                    </Grid>

                    {/*second top right box seprated by another grid  */}
                    <Grid item xs={12} md={4} lg={3}>
                        <Paper className={fixedHeightPaper}>
                            <Typography>
                                Your Balance is: </Typography><Link>$600,000</Link>
                            <Typography>Main Account:</Typography><Link>$452,000</Link>
                            <Typography>Sub Account:</Typography><Link>$148,000</Link>
                        </Paper>
                    </Grid>

                    {/* last grid lower part of the page with one paper on top */}
                    <Grid item xs={12}>
                        <Paper className={classes.paper}>

                            <div>
                                <p> Time : {date.toLocaleTimeString()}</p>
                                <p> Date : {date.toLocaleDateString()}</p>

                            </div>


                        </Paper>
                    </Grid>
                </Grid>
                <br></br>
                <Link href="#" variant="" onClick={logOut}>
                    Click here to log out
                </Link>


            </Container>

        </div>
    );
}