import React from 'react'
import { useHistory } from 'react-router';
import { Button, Typography, makeStyles, Grid, Paper } from '@material-ui/core'


const useStyles = makeStyles((theme) => ({
    root: {
        height: '100vh',
        backgroundColor: 'orange',
    },
    image: {
        backgroundImage: 'url(https://www.capitaland.com/content/dam/capitaland-media-library/integrateddevelopment-urbandevelopment/Singapore/Singapore/plaza-8-cbp/Plaza8CBP-3.jpg.transform/cap-midres/image.jpg)',
        backgroundRepeat: 'no-repeat',
        backgroundColor:
            theme.palette.type === 'light' ? theme.palette.grey[50] : theme.palette.grey[900],
        backgroundSize: 'cover',
        backgroundPosition: 'center',
    },
    paper: {
        margin: theme.spacing(8, 4),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    

    form: {
        width: '100%',
        marginTop: theme.spacing(1),
    },
    submit: {
        "&:hover": {
            backgroundColor: '#B4B3B3'
          },
        margin: theme.spacing(3, 0, 2),
        backgroundColor: '#9A9A9A'
    },
    colorBack:{
        backgroundColor: 'orange'
    }
}));

export default function HomePage() {
    const classes = useStyles();
    var history = useHistory();


    var handleLoginSwitch = (e) => {

        history.push("/LoginPage");
        e.preventDefault();

    }

    var handleRegisterSwitch = (e) => {

        history.push("/RegisterPage");
        e.preventDefault();
    }

    return (

    
        <Grid container component="main" className={classes.root} >

            <Grid item xs={false} sm={4} md={7} className={classes.image} />
            <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square className={classes.colorBack}>

                <div className={classes.paper}>

                    <Typography component="h1" variant="h2" color='primary' >
                        Optimum DigiBanking
                    </Typography>
                    <form className={classes.form} noValidate>

                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            color="primary"
                            className={classes.submit}
                            onClick={handleLoginSwitch}
                        >
                            Log In
                        </Button>


                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            color="primary"
                            onClick={handleRegisterSwitch}
                            className={classes.submit}
                        >
                            Register
                        </Button>

                    </form>
                </div>
            </Grid>
        </Grid>
    );
}