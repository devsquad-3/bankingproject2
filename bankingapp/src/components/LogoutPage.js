import React from 'react';
import { Grid, Link, Typography } from '@material-ui/core/';
import { makeStyles } from '@material-ui/core/styles';
import { useHistory } from 'react-router';

const useStyles = makeStyles((theme) => ({
    paper: {
        margin: theme.spacing(40, 80),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    link: {
        fontSize: 14,
        alignItems: 'center',
    },
    linklink: {
        fontSize: 29
    },
    cols: {
        backgroundColor: 'orange',
        height: '100vh'
    }
}));

export default function SignInSide() {
    const classes = useStyles();
    var history = useHistory();


    var backToHome = (e) => {

        history.push("/HomePage");
        e.preventDefault();
    }

    return (
        <Grid container className={classes.cols}>
            <div className={classes.paper}>
                <Typography component="h1" variant="h5" className={classes.linklink} >
                    Thank You For Banking With Us! :)
                </Typography>
                <form  >
                    <Link href="#" onClick={backToHome} variant="body2" className={classes.link}>
                        Click here to go to Home Page
                    </Link>
                </form>
            </div>
        </Grid>

    );
}