import './App.css';
import LoginPage from './components/LoginPage';
import RegisterPage from './components/RegisterPage';
import LogoutPage from './components/LogoutPage';
import HomePage from './components/HomePage';
import ForgotPassword from './components/ForgotPassword';
import EmailConfirmation from './components/EmailConfirmation';
import AdminPage from './components/AdminPage';
import AccountPage from './components/AccountPage';
import PasswordResetPage from './components/PasswordResetPage';
import {BrowserRouter as Router, Switch, Route,Redirect} from 'react-router-dom'

function App() {
  return (

    <div>
      <Router>
      <Switch>
        <Route path='/HomePage'>
          <HomePage></HomePage>
        </Route>

        <Route path='/LoginPage'>
          <LoginPage></LoginPage>
        </Route>

        <Route path='/RegisterPage'>
        <RegisterPage></RegisterPage>
        </Route>

        <Route path='/LogoutPage'>
          <LogoutPage></LogoutPage>
        </Route>

        <Route path='/ForgetPassword'>
          <ForgotPassword></ForgotPassword>
        </Route>

        <Route path='/Confirm'>
          <EmailConfirmation></EmailConfirmation>
        </Route>

        <Route path='/AdminPage'>
          <AdminPage></AdminPage>
        </Route>

        <Route path='/AccountPage'>
          <AccountPage></AccountPage>
        </Route>
        <Route path='/ResetPassword&userid=:id'>
          <PasswordResetPage></PasswordResetPage>
          </Route>
        <Route exact path='/'>
          <Redirect to='/HomePage'></Redirect> 
        </Route>

      </Switch>
      </Router>

    </div>
    
  );
}

export default App;
