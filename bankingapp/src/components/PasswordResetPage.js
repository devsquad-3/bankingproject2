import React from 'react'
import { useHistory } from 'react-router'
import { Avatar, Button, TextField, Link, Grid, Typography, Container ,Box} from '@material-ui/core/';
import { makeStyles } from '@material-ui/core/styles';
import data from "./data2.json"
import axios from 'axios';


export default function PasswordResetPage() {
    var userid=parseInt(new URLSearchParams(window.location.href).get("userid"));
    const accountList=data.Accounts;
    var history = useHistory();
    
    var db_username=accountList.map(username=>username.Username);
    var db_password=accountList.map(password=>password.Password);

    const handlePassword=()=>{
        
        var pass1=document.getElementById("password1").value;
        var pass2=document.getElementById("password2").value;
        var passwordRegex=/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8}$/
        console.log(db_password.includes(!passwordRegex.test(pass1)))
        if(passwordRegex.test(pass1)===false){
            document.getElementById("checkpass1").innerHTML="Please enter valid password";
        }
        if(passwordRegex.test(pass1)===true){
            document.getElementById("checkpass1").innerHTML="";
        }
        if(db_password.includes(pass1)){
            document.getElementById("checkpass1").innerHTML="Please enter another password";
        }
        if(pass1!==pass2){
            document.getElementById("checkpass2").innerHTML="Please re-enter password";
        }
        else{
            document.getElementById("checkpass1").innerHTML="";
            document.getElementById("checkpass2").innerHTML="";
        }


    }
    var handleSubmitInfo = (e) => {

        var name="";
        var email="";
        var pass2=document.getElementById("password2").value;
        for(var i=0; i<db_username.length; i++){
            if(accountList[i].id===userid){
                name=accountList[i].Username;
                email=accountList[i].Email;
            }
        }

        axios.put('http://localhost:3001/Accounts/'+ userid , {
            Username: name,
            Email: email,
            Password: pass2,
            Group: "user",
            Active: true
          })
          .then(function (response) {
            console.log(response);
          })
          .catch(function (error) {
            console.log(error);
          });
        alert("Password successfully reset, U will be redirected to the Homepage")
        history.push("/Homepage");

    }

    const useStyles = makeStyles((theme) => ({
        paper: {
            marginTop: theme.spacing(8),
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
        },
        avatar: {
            margin: theme.spacing(1),
            backgroundColor: theme.palette.primary.main,
        },
        form: {
            width: '100%', // Fix IE 11 issue.
            marginTop: theme.spacing(1),
        },
        submit: {
            "&:hover": {
                backgroundColor: '#B4B3B3'
              },
            margin: theme.spacing(3, 0, 2),
            backgroundColor: '#9A9A9A'
        
        },
        cols:{
            backgroundColor: 'orange',
            height: '100vh'
        },

        error:{
            color:"red"
        },
        textfieldColor:{
        
            backgroundColor: 'white'
        },
    }));

    const classes = useStyles();


    return (
        <Grid container className={classes.cols}>
        <Container container component="main" maxWidth="xs" className={classes.cols}>
            
            <div className={classes.paper} >
                <Avatar className={classes.avatar}>
                    Opt
                </Avatar>
                <Typography component="h1" variant="h3">
                    Please key in your new password
                </Typography>
                <form className={classes.form}>
                {/* <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        id="email"
                        label="email"
                        onChange={handlePassword}
                    />
                    <Box id="checkEmail" onChange={handlePassword}/> */}
                    <TextField
                        variant="filled"
                        margin="normal"
                        required
                        type="password"
                        fullWidth
                        id="password1"
                        label="Password"
                        onChange={handlePassword}
                        className={classes.textfieldColor}
                    />
                    <Box id="checkpass1" className={classes.error} onChange={handlePassword}/>
                    <TextField
                        variant="filled"
                        margin="normal"
                        required
                        type="password"
                        fullWidth
                        label="Re-enter new password"
                        id="password2"
                        onChange={handlePassword}
                        className={classes.textfieldColor}
                    
                    />
                     <Box id="checkpass2" className={classes.error} onChange={handlePassword}/>
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                        onClick={handleSubmitInfo}
                    >
                        Reset Password
                    </Button>
                    <Grid container>
                        <Grid item xs>
                        </Grid>
                    </Grid>
                </form>
            </div>
           
        </Container>
        </Grid>
    );
}