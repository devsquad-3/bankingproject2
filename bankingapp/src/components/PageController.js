import React from 'react'
import LoginPage from './LoginPage'
import RegisterPage from './RegisterPage';

var currpage = 1;


function SwitchPage(){


switch(currpage)
{
    case 1: 
    return(
        <div>
            <LoginPage></LoginPage>
        </div>
    );

    case 2:
        return(
            <div>
                <RegisterPage></RegisterPage>
            </div>
        )

    default:
        break;
}

}


function PageController(){

    return(
        <div>
            <SwitchPage></SwitchPage>
        </div>
    )
}

export default PageController;