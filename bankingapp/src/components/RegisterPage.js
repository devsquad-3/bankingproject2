import React from 'react'
import { useHistory } from 'react-router';
import { Button, Typography } from '@material-ui/core'
import { makeStyles } from "@material-ui/core";
import Grid from '@material-ui/core/Grid';
import { Paper } from '@material-ui/core';
import { Avatar } from '@material-ui/core';
import { TextField } from '@material-ui/core';
import { Box } from '@material-ui/core';
import { Link } from '@material-ui/core';
import emailjs from 'emailjs-com';
import axios from 'axios';
import data from './data2.json'



const useStyles = makeStyles((theme) => ({
    root: {
        height: '100vh',
    },
    image: {
        backgroundImage: 'url(https://www.capitaland.com/content/dam/capitaland-media-library/integrateddevelopment-urbandevelopment/Singapore/Singapore/plaza-8-cbp/Plaza8CBP-3.jpg.transform/cap-midres/image.jpg)',
        backgroundRepeat: 'no-repeat',
        backgroundColor:
            theme.palette.type === 'light' ? theme.palette.grey[50] : theme.palette.grey[900],
        backgroundSize: 'cover',
        backgroundPosition: 'center',
    },
    paper: {
        margin: theme.spacing(8, 4),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },

    colorBack: {
        backgroundColor: 'orange'
    },

    form: {
        width: '100%',
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
        backgroundColor: '#9A9A9A'
    },
    error1: {
        color: 'red'
    },
    Avatar: {
        margin: theme.spacing(1),
        backgroundColor: 'blue',
    }
}));


export default function RegisterPage() {

    const accountList=data.Accounts;
    const db_Email=accountList.map(account=>account.Email)
    const classes = useStyles();
    const history = useHistory();


    var handleLoginSwitch = (e) => {

        history.push("/LoginPage");
        e.preventDefault();

    }
    const checkPass = (event) => {

        const passwordRegex = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8}$/
        var password = document.getElementById("password1").value;
        var password1 = document.getElementById("password2").value;
        console.log(passwordRegex.test(password))
        if (passwordRegex.test(password) === false) {
            document.getElementById("passwordCheck").innerHTML = "Please enter valid password"

        }
        if (passwordRegex.test(password) === true) {
            document.getElementById("passwordCheck").innerHTML = ""
        }
        if ((password !== password1) === true) {

            document.getElementById("passwordCheck1").innerHTML = "Please retype password"
        }
        if ((password === password1) === true) {
            document.getElementById("passwordCheck1").innerHTML = ""
        }

        event.preventDefault();
    }


    const checkChange = (event) => {

        const emailRegex=/^(([^<>()\]\\.,;:\s@"]+(\.[^<>()\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        var email= document.getElementById("Email").value;


        if(!emailRegex.test(email)){
            document.getElementById("emailCheck").innerHTML="Please enter valid email"
        }
        if(db_Email.includes(email)){
            document.getElementById("emailCheck").innerHTML="Email has been used"
        }
        else{
            document.getElementById("emailCheck").innerHTML=""
        }
        event.preventDefault();

    }

    const checkInput = (event) => {

        var name=document.getElementById("Name").value;
        if(!name){
            document.getElementById("checkName").innerHTML="*Please enter name"
        }
        else{
            document.getElementById("checkName").innerHTML=""
        }
        event.preventDefault();
    
      }
    
    const Registering=(event)=>{

        var name=document.getElementById("Name").value;
        var Email=document.getElementById("Email").value;
        var password1=document.getElementById("password1").value;
        var password2=document.getElementById("password2").value;
        
        if(!name){
            alert("Please enter Name");
        }
        if (!Email) {
            alert("Please enter Email");
        }
        
        if(!password1||!password2){
            alert("Please enter password");
        }
        else if (password1 !== password2) {
            alert("Please retype password");
        }

        else{

            const email={
                email: Email,
                name: name
            }
            event.preventDefault();

            axios.post('http://localhost:3001/Accounts', {
                Username: name,
                Email: Email,
                Password: password1,
                Group: "user",
                Active: false
              })
              .then(function (response) {
                console.log(response);
              })
              .catch(function (error) {
                console.log(error);
              });

            emailjs.send('verification_service', 'template_rk0rwug', email,'user_9O9UCsHHzTZrD3bO3mHjj')
            .then((result) => {
                    console.log(result.text);
                    }, (error) => {
                     console.log(error.text);
                     });
    
            history.push('/LoginPage');

        }
        event.preventDefault();

    }




    return (
        <Grid container component="main" className={classes.root}>
            <Grid item xs={false} sm={4} md={7} className={classes.image} />
            <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square className={classes.colorBack}>
            <div className={classes.paper}>
                {/* to adjust the attributes of the avatar need go to the setsytle function */}
                <Avatar className={classes.Avatar} >
                    Opt
                </Avatar>
                {/* just another header */}
                <Typography component="h1" variant="h5">
                    Sign Up Here
                </Typography>
                {/* alignment/space of the grids below */}
                <form className={classes.form} onSubmit={Registering}>
                    {/* first main grid for the add name field */}
                    <Grid container spacing={2}>
                        {/* grid length */}
                        <Grid item xs={12} >
                            <TextField
                                variant="filled"
                                required
                                fullWidth
                                id="Name"
                                label="Full Name"
                                onChange={checkInput}
                            />
                            <Box id="checkName" className={classes.error1} onChange={checkInput}/>
                            {/* second main grid */}
                        </Grid>
                        {/* actual grid length */}
                        <Grid item xs={12}>
                            <TextField
                                variant="filled"
                                required
                                fullWidth
                                id="Email"
                                label="Email"
                                onChange={checkChange}
                            />
                             <Box id="emailCheck" className={classes.error1} onChange={checkChange}></Box>
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                variant="filled"
                                required
                                fullWidth
                                label="Password"
                                type="password"
                                id="password1"
                                onChange={checkPass}
                            />
                             <Box id="passwordCheck" className={classes.error1} onChange={checkPass}/>
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                variant="filled"
                                required
                                fullWidth
                                label="Confirm Password"
                                type="password"
                                id="password2"
                                onChange={checkPass}
                            />
                             <Box id="passwordCheck1" className={classes.error1} onChange={checkPass}/>
                        </Grid>
                        {/* grid to separate the checkbox else it will overlap and wont look pleasing */}
                        {/* <Grid item xs={0}>
                            <FormControlLabel
                                control={<Checkbox value="allowExtraEmails" color="primary" />}
                                label="I want to receive emails for Marketing Promotions"
                            />
                        </Grid> */}
                        </Grid>
                        {/* submit button so can add the onsubmit or onclick function here or at the form div */}
                        <Button
                            type="submit"
                            fullWidth
                            variant='standard'
                            color="primary"
                            className={classes.submit}
                            
                        >
                            Sign Up
                        </Button>
                        {/* last grid for the have account button/Link */}
                        <Grid container justify="flex-end">
                            <Grid item>
                                <Link href="#" variant="body2" onClick={handleLoginSwitch}>
                                    Already have an account? Sign in
                                </Link>
                            </Grid>
                        </Grid>

                    </form>
                </div>
            </Grid>
        </Grid>
    );
}