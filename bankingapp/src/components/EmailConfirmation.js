import React from 'react';
import { useHistory } from "react-router";
import { Link, Grid, Typography } from '@material-ui/core/';
import { makeStyles } from '@material-ui/core/styles';

export default function EmailConfirmation() {

    var history = useHistory();

    var handleonClick = (e) => {

        history.push("/HomePage")
    }

    const useStyles = makeStyles((theme) => ({
        root: {
            height: '100vh',
        },
        image: {
            backgroundImage: 'url(https://www.capitaland.com/content/dam/capitaland-media-library/integrateddevelopment-urbandevelopment/Singapore/Singapore/plaza-8-cbp/Plaza8CBP-3.jpg.transform/cap-midres/image.jpg)',
            backgroundRepeat: 'no-repeat',
            backgroundColor:
                theme.palette.type === 'light' ? theme.palette.grey[50] : theme.palette.grey[900],
            backgroundSize: 'cover',
            backgroundPosition: 'center',
            height: '60vh'
        },
        paper: {
            height: '40vh',
            width: "100%",
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            backgroundColor: 'orange',
            
        },
        colorBack:{
            backgroundColor: 'orange'
        },
        link: {
            fontSize: 18
        }


    }));

    const classes = useStyles();

    return (
        <Grid container component="main" className={classes.root}>
            <Grid item xs={false} sm={6} md={12} className={classes.image} />
            <Grid  elevation= {2} className={classes.paper} >
                <Typography variant="h5" color="textSecondary">
                    A confirmation Email has been sent, Please check your email for the verification code.
                    Thank you!
                </Typography>

                <form className={classes.form} >

                    <br></br>
                    <Link href="#" variant='body2' className={classes.link} onClick={handleonClick}>
                        Click here to go back to Home Page
                    </Link>



                </form>
            </Grid>

        </Grid>
    );
}