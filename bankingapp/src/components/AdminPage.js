import React from 'react';
import { useHistory } from 'react-router';
import { Button, Grid, Link, Typography, AppBar, Toolbar, Container, Paper } from '@material-ui/core/';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import data from "./data2";
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import axios from 'axios';


const useStyles = makeStyles((theme) => ({

    appBarSpacer: theme.mixins.toolbar,
    content: {
        flexGrow: 1,
        height: '100vh',
        overflow: 'auto',
        
    },

    container: {
        paddingTop: theme.spacing(4),
        paddingBottom: theme.spacing(4),
    },

    paper: {
        padding: theme.spacing(1),
        display: 'flex',
        overflow: 'auto',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#ff4081'

    },

    fixedHeight: {
        height: 400,
    },

    tableColor:{
        backgroundColor: 'pink',
    }


}));


export default function AdminPage() {


    var history = useHistory();
    const classes = useStyles();
    const fixedHeightPaper = clsx(classes.paper, classes.fixedHeight);




    var logOut = (e) => {

        history.push("/LogoutPage");
        e.preventDefault();
    }
    //can create a delete account function (will add onClick={deleteAcct})
    var deactivateAcct = (e) => {
        var email = document.getElementById("demo-simple-select").value
        var userid=0;
        var active=true;
        accountList.map((row) => {
            if (row.Email === email) {
                userid=row.id;
                active=row.Active;
                 axios.put('http://localhost:3001/Accounts/'+userid, {
                Username: row.Username,
                Email: row.Email,
                Password: row.Password,
                Group: "user",
                Active: (!active)
              })
              .then(function (response) {
                console.log(response);
              })
              .catch(function (error) {
                console.log(error);
              });
            }
            
           


        })
        if(active){
            alert("Account has been deactivated.");
        }
        else{
            alert("Account has been activated.");
        }
        
        e.preventDefault();
    }
    const accountList = data.Accounts;

    return (
        <div>
            {/* the dashboard header(need realign back to center but cant seem to work) */}
            <AppBar position="fixed" color="secondary">
                <Toolbar >
                    <Typography component="h1" variant="h6" color="inherit" >
                        Admin Dashboard
                    </Typography>
                </Toolbar>
            </AppBar>


            <main className={classes.content}>
                <div className={classes.appBarSpacer} />
                <Container maxWidth="lg" className={classes.container}>
                    {/* this is the main grid  */}
                    <Grid container spacing={10}>

                        {/* first top left box on the page */}
                        <Grid item xs={12} md={10} lg={6}>
                            <Paper elevation={3} className={fixedHeightPaper}>
                                <Typography color='primary'>
                                    Welcome back to your Dashboard!

                                </Typography>
                                <br></br>
                                <Typography>
                                    User's List
                                </Typography>
                                <br></br>
                                <TableContainer style={{ width: 560 }} component={Paper}>
                                    <Table  className={classes.tableColor} aria-label="simple table">
                                        <TableHead>
                                            <TableRow>
                                                <TableCell align="right">ID </TableCell>
                                                <TableCell align="right">Username</TableCell>
                                                <TableCell align="right">Email &nbsp;</TableCell>
                                                <TableCell align="right">Password&nbsp;</TableCell>
                                                <TableCell align="right">Active&nbsp;</TableCell>
                                            </TableRow>
                                        </TableHead>
                                        <TableBody>
                                            {accountList.map((row) => (
                                                <TableRow key={row.id}>
                                                    {/* <TableCell component="th" scope="row">
                                                    </TableCell> */}
                                                    <TableCell align="right">{row.id}</TableCell>
                                                    <TableCell align="right">{row.Username}</TableCell>
                                                    <TableCell align="right">{row.Email}</TableCell>
                                                    <TableCell align="right">{row.Password}</TableCell>
                                                    <TableCell align="right">{String(row.Active)}</TableCell>
                                                </TableRow>
                                            ))}
                                        </TableBody>
                                    </Table>
                                </TableContainer>
                            </Paper>
                        </Grid>

                        {/*second top right box seprated by another grid  */}
                        <Grid align='center' item xs={12} md={4} lg={6}>
                            <Paper  elevation={3} className={fixedHeightPaper} >
                                <Typography color='primary'>
                                    Search User Accounts
                                </Typography>
                                <br></br>
                                <InputLabel id="demo-simple-select-label">Input Email Here</InputLabel>
                                <Select
                                    native defaultValue=""
                                    id="demo-simple-select"
                                >
                                    {accountList.map((row) => (
                                         <option key={row.ID}>{row.Email}</option>
                                    ))}
                                </Select>
                                <br></br>
                                <Button variant='contained' color='secondary' onClick={deactivateAcct}>
                                    Activate/Deactivate this account?
                                </Button>

                                <br></br>
                                <br></br>
                                <Link href='#' variant='' >
                                    Account Aprroval
                                </Link>
                            </Paper>
                        </Grid>

                        {/* not sure if we need this last grid and paper */}
                        <Grid item xs={12}>
                            <Paper elevation={3} className={classes.paper}>
                                <Typography color='primary'>
                                    Optimum Digibanking - Welcome back Admin!
                                </Typography>
                            </Paper>
                        </Grid>
                    </Grid>
                    <br></br>
                    <Link href="#" variant="" onClick={logOut}>
                        Log Out
                    </Link>

                </Container>
            </main>
        </div>
    );
}