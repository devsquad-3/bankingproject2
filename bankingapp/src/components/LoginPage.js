import React from 'react';
import data from "./data2";
import { useHistory } from 'react-router';
import { Avatar, Button, TextField, FormControlLabel, Checkbox, Link, Paper, Box, Grid, Typography } from '@material-ui/core/';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    root: {
        height: '100vh',
    },
    image: {
        backgroundImage: 'url(https://www.capitaland.com/content/dam/capitaland-media-library/integrateddevelopment-urbandevelopment/Singapore/Singapore/plaza-8-cbp/Plaza8CBP-3.jpg.transform/cap-midres/image.jpg)',
        backgroundRepeat: 'no-repeat',
        backgroundColor:
            theme.palette.type === 'light' ? theme.palette.grey[50] : theme.palette.grey[900],
        backgroundSize: 'cover',
        backgroundPosition: 'center',
    },
    paper: {
        margin: theme.spacing(8, 4),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },

    colorBack:{
        backgroundColor: 'orange'
    },

    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.primary.main,

    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    submit: {
        "&:hover": {
            backgroundColor: '#B4B3B3'
          },
        margin: theme.spacing(3, 0, 2),
        backgroundColor: '#9A9A9A'
        
    },

    textfieldColor:{
        
        backgroundColor: 'white'
    },

    buttonColor:{
        
    },

    link: {
        fontSize: 16
    }

}));

const errorStyle = {

    color: 'red'

};

export default function LoginPage() {

    const accountList = data.Accounts;
    const db_email = accountList.map(account=>account.Email);
    const db_password= accountList.map(account=>account.Password);
    const db_group = accountList.map(account=>account.Group);
    const db_active = accountList.map(account=>account.Active);

    const classes = useStyles();
    var history = useHistory();

    var backToHome = (e) => {

        history.push("/HomePage");
        e.preventDefault();
    }

    var HandlePasswordChange=(e)=>{

        var password = document.getElementById("password").value;
        if(!password)
        {
            document.getElementById("error_password").innerHTML = "*Please input your password";
        }
        else
        {
            document.getElementById("error_password").innerHTML = " ";
        }

    }

    var HandleEmailChange=(e)=>{
        //check if email typed out is a valid email formatt
        let testEmail = /^(([^<>()\]\\.,;:\s@"]+(\.[^<>()\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if(testEmail.test(document.getElementById("email").value))
        {
            document.getElementById("error_email").innerHTML = " ";
        }

        else
        {
            document.getElementById("error_email").innerHTML = "*Please input valid email";
            //document.getElementById("email").style.outline = 'red';
        }
        

    }

    var HandleSubmit = (e) => {
        
        var authen = false;
        var accActive = true;
        var email = document.getElementById("email").value;
        var password = document.getElementById("password").value

        for(var i=0; i < db_email.length; i++)
        {
            if(db_email[i] === email && db_password[i] === password)
            {
                if(db_active[i] === false)
                {
                    accActive = false;
                    break;
                }

                if(db_group[i] === "admin")
                {
                    history.push("/AdminPage");
                    authen = true;
                    break;
                }

                else
                {
                   
                    history.push("/AccountPage");
                    authen = true;
                    break;
                }

                
            }

        }

        if(!authen && accActive)
        {
            alert("Login is invalid");
        }

        else if(!authen && !accActive)
        {
            alert("Account has been deactivated.");
        }

        e.preventDefault();


    }

    var HandleForgetClick = (e) => {
        history.push("/ForgetPassword");
    }

    return (
        // main grid use to add picture to half of the grid
        <Grid container component="main" className={classes.root}>
            <Grid item xs={false} sm={4} md={7} className={classes.image} />
            <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square className={classes.colorBack}>
                <div className={classes.paper}>
                    {/* avatar icon */}
                    <Avatar className={classes.avatar}>
                        Opt
                    </Avatar>
                    {/* log in header */}
                    <Typography component="h1" variant="h5">
                        Log In/Sign In
                    </Typography>
                    {/* Email and password inputfields */}
                    <form onSubmit={HandleSubmit} className={classes.form} >
                        <TextField
                            variant="filled"
                            margin="normal"
                            fullWidth
                            id="email"
                            label="Email"
                            placeholder="Email"
                            onKeyPress={HandleEmailChange}
                            className={classes.textfieldColor}
                        />
                        <span id="error_email" style={errorStyle}/>

                        <TextField
                            variant="filled"
                            margin="normal"
                            fullWidth
                            id="password"
                            label="Password"
                            type="password"
                            placeholder="Password"
                            onKeyPress={HandlePasswordChange}
                            className={classes.textfieldColor}
                        />
                        <span id="error_password" style={errorStyle}/>
                        <br></br>

                        {/* remember me icon */}
                        <FormControlLabel
                            control={<Checkbox value="remember" color="white" />}
                            label="Remember me"
                            color='white'
                        />
                        {/* log in button icon */}
                        <Button
                            onClick={HandleSubmit}
                            type="submit"
                            fullWidth
                            variant="standard"
                            color="primary"
                            className={classes.submit}
                        >
                            Log In
                        </Button>
                        {/* forgot password icon */}
                        <Grid container>
                            <Grid item xs>
                                <Link onClick={HandleForgetClick} href="#" variant="body2" className={classes.link}>
                                    Forgot password?
                                </Link>
                            </Grid>
                            <Grid item xs>

                            </Grid>
                            <Grid item>

                            </Grid>
                        </Grid>
                        {/* back to home page icon */}
                        <Box mt={5}>
                            <Link href="#" variant="" onClick={backToHome} className={classes.link}>
                                Go back to Home Page
                            </Link>
                        </Box>
                    </form>
                </div>
            </Grid>
        </Grid>
    );
}